# frozen_string_literal: true

class Company
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps

  paginates_per 10

  field :tag, type: String
  field :title, type: String

  has_many :posts, as: :posts, inverse_of: :company, dependent: :nullify
  has_many :users, as: :users, inverse_of: :company, dependent: :nullify

  validates :tag, uniqueness: true
  validates :tag, :title, presence: true
end
