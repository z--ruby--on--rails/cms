# frozen_string_literal: true

class User
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps

  paginates_per 10

  # :reek:Attribute
  attr_accessor :password, :password_confirmation

  protected

  field :pseudonym, type: String
  field :name, type: String
  field :email, type: String
  field :passhash, type: String
  field :passsalt, type: String

  belongs_to :company, dependent: :nullify, index: true

  validates :password, confirmation: true
  validates :password, presence: { on: :create }
  validates :pseudonym, :email, uniqueness: true
  validates :pseudonym, :name, :email, presence: true

  before_create :encrypt_password

  set_callback(:create, :before) do |_doc|
    encrypt_password
  end

  def encrypt_password
    return if password.blank?

    self.passsalt = BCrypt::Engine.generate_salt
    self.passhash = BCrypt::Engine.hash_secret(password, passsalt)
  end
end
