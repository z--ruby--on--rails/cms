# frozen_string_literal: true

class Post
  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps

  paginates_per 10

  field :slug, type: String
  field :title, type: String
  field :text, type: String
  field :keyword, type: String

  belongs_to :company, index: true
  belongs_to :user, index: true

  validates :slug, uniqueness: true
  validates :slug, :title, :text, presence: true
end
