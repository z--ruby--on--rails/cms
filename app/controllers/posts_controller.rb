# frozen_string_literal: true

# :reek:InstanceVariableAssumption
class PostsController < ApplicationController
  include ApplicationHelper

  before_action :set_post, only: %i[show edit update destroy]

  # GET /posts
  def index
    page_num = params[:page]
    @posts = Post.where(deleted_at: nil).page page_num
    @posts_archive = Post.where(:deleted_at.ne => nil).page page_num
  end

  # GET /posts/:slug
  def show; end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/:slug/edit
  def edit; end

  # POST /posts
  def create
    original_params = post_params.merge!(company_id: current_user.company_id, user_id: current_user._id)
    @post = Post.new(original_params)

    if @post.save
      redirect_to posts_path, notice: 'Post was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /posts/:slug
  def update
    return unless access_by_user_id(@post.user_id)

    if @post.update(post_params)
      redirect_to posts_path, notice: 'Post was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /posts/:slug
  def destroy
    return unless access_by_user_id(@post.user_id)

    @post.destroy
    redirect_to posts_url, notice: 'Post was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = post(params[:slug])
  end

  # Only allow a trusted parameter "white list" through.
  def post_params
    params.require(:post).permit(:slug, :text, :title, :keyword, :company_id)
  end
end
