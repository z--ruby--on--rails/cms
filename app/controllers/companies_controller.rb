# frozen_string_literal: true

# :reek:InstanceVariableAssumption
class CompaniesController < ApplicationController
  include ApplicationHelper

  before_action :set_company, only: %i[show edit update destroy]

  # GET /companies
  def index
    page_num = params[:page]
    @companies = Company.where(deleted_at: nil).page page_num
    @companies_archive = Company.where(:deleted_at.ne => nil).page page_num
  end

  # GET /companies/:tag
  def show; end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/:tag/edit
  def edit; end

  # POST /companies
  def create
    @company = Company.new(company_params)

    if @company.save
      redirect_to companies_path, notice: 'Company was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /companies/:tag
  def update
    return unless access_by_user_id(0)

    if @company.update(company_params)
      redirect_to companies_path, notice: 'Company was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /companies/:tag
  def destroy
    return unless access_by_user_id(0)

    @company.destroy
    redirect_to companies_url, notice: 'Company was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_company
    @company = company(params[:tag])
  end

  # Only allow a trusted parameter "white list" through.
  def company_params
    params.require(:company).permit(:tag, :title)
  end
end
