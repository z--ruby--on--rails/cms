# frozen_string_literal: true

class ApplicationController < ActionController::Base
  helper_method :current_user

  private

  def current_user
    uid = session[:user_id]
    @current_user ||= User.find(uid) if uid
  end
end
