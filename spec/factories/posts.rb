# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    text { Faker::String.random(length: [1, (2..5), [3, 6], nil]) }
    slug { Faker::Internet.slug }
    keyword { '' }
  end
end
