# frozen_string_literal: true

Rails.application.routes.draw do
  resources :companies, param: :tag do
    resources :users, only: %i[index new create], controller: 'users'
    resources :posts, only: [:index], controller: 'posts'
  end

  resources :users, param: :pseudonym do
    resources :posts, controller: 'posts'
  end

  resources :posts, param: :slug

  get 'log_out' => 'sessions#destroy', as: 'log_out'
  get 'log_in' => 'sessions#new', as: 'log_in'
  get 'sign_up' => 'users#new', as: 'sign_up'

  resources :sessions

  root 'posts#index'
end
