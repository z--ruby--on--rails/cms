# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.1'

gem 'rails', '~> 6.0.2.2'

gem 'bcrypt', require: 'bcrypt'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'bson_ext'
gem 'kaminari-actionview'
gem 'kaminari-mongoid'
gem 'mongoid', '~> 7.0.5'
gem 'mongoid_paranoia'
gem 'puma', '~> 4.1'

gem 'active_model_serializers'

gem 'hamlit-rails'
gem 'i18n-js'
gem 'sass-rails', '~> 5'
gem 'turbolinks', '~> 5'
gem 'webpacker', '~> 4.0'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'rspec-rails'
end

group :development do
  gem 'annotate'
  gem 'brakeman'
  gem 'haml_lint', require: false
  gem 'listen'
  gem 'reek'
  gem 'rubocop'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'spring'
  gem 'spring-watcher-listen'
  gem 'web-console'
end

group :test do
  gem 'capybara'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'hashdiff'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
  gem 'webmock'
end
